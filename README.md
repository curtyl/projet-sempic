# Projet SEMPIC

Service de gestion, de partage et d’annotation de photos.

## Introduction

Dans une invite de commande, placez vous sur le chemin du dossier Sempic. Puis tapez cette commande : 

	mvn clean package tomee:run

Pour avoir une vue de base de donnée, vous pouvez tapez cette commande :

    java -cp target/apache-tomee/lib/hsqldb-2.3.2.jar org.hsqldb.util.DatabaseManagerSwing
    
Et choisir dans le champs URL la ligne suivante :

    jdbc:hsqldb:file:./target/app_database;hsqldb.lock_file=false

## Application

Pour tester l'application, vous pouvez faire des requêtes HTTP sur l'url suivante : http://localhost:8080/data/
Puis en accord avec les méthodes disponibles dans le document des spécifications présent dans le dossier Spec/specif_api_sempic.odt, vous pouvez exécuter différentes méthodes.
