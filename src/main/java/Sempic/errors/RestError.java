package Sempic.errors;

public class RestError {

    public String type;
    public String title;
    public String  detail;
    public String instance;

    public RestError() {}

    public RestError(String type, String title, String detail, String instance) {
        this.type = type;
        this.title = title;
        this.detail = detail;
        this.instance = instance;
    }
}
