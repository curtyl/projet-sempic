package Sempic.errors;

import Sempic.model.UserModelException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UserModelExceptionMapper implements ExceptionMapper<UserModelException> {
    @Override
    public Response toResponse(UserModelException exception) {
        RestError e = new RestError();
        e.type="/model-errors";
        e.title = "Model Error";
        e.detail = exception.getMessage();
        return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
    }
}
