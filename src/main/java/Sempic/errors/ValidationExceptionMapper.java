package Sempic.errors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException exception) {

        List<RestError> res = new ArrayList<>();

        for ( ConstraintViolation<?> v : exception.getConstraintViolations()) {
            RestError e = new RestError();
            e.type="/validation-errors";
            e.title = "Validation error";
            e.detail = v.getPropertyPath()+" "+v.getMessage();
            res.add(e);

        }
        return Response.status(Response.Status.BAD_REQUEST).entity(res).build();

    }
}
