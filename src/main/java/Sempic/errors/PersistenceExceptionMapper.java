package Sempic.errors;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.sql.SQLIntegrityConstraintViolationException;

/**
 * Cette classe permet de gÃ©nÃ©rer les exceptions du framework de persistence
 *
 */
@Provider
public class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException> {
    @Override
    public Response toResponse(PersistenceException exception) {

        // on regarde d'abord si exception est une sous classe de PersistenceException
        // les sous-classes possible sont dÃ©taillÃ©es sur https://docs.oracle.com/javaee/7/api/javax/persistence/PersistenceException.html
        // dans le cas ou l'instance n'est pas trouvÃ©e on a une EntityNotFoundException,
        // et on renvoie une rÃ©ponse 404
        if (exception instanceof EntityNotFoundException) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        RestError e = new RestError();
        e.type="/persistence-errors";
        e.title = "Persistence Error";
        e.detail = analyseSQLException(exception);;

        return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
    }


    /**
     * MÃ©thode qui permet de "fouiller" dans la pile d'exception et de trouver des erreurs
     * spÃ©cifique Ã  SQL qui ne sont pas gÃ©rÃ©es par des PersistenceException spÃ©cifiques.
     * C'est utile par exxemple pour les violations de contraintes d'unicitÃ©.
     * @param t : l'erreur Ã  analyser
     * @return un texte de l'erreur
     */
    private static String analyseSQLException(PersistenceException e)  {
        Throwable t = e.getCause();
        while (t != null) {
            if (t instanceof SQLIntegrityConstraintViolationException) {
                SQLIntegrityConstraintViolationException sqlEx = (SQLIntegrityConstraintViolationException) t;
                if ("23505".equals(sqlEx.getSQLState())) {
                    return "Valeur dupliquée";
                }
            }
            t = t.getCause();
        }
        return e.getMessage();
    }
}
