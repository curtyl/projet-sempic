package Sempic.model;

/**
 * Classe utilisÃ©e pour gÃ©rer des erreurs propres aux classes du modÃ¨le.
 * Je ne l'ai pas utilisÃ© pour l'instant.
 */
public class UserModelException extends RuntimeException {
    public UserModelException(String s) {
        super(s);
    }
}
