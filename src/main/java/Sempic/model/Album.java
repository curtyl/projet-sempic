package Sempic.model;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.Objects;
import java.util.Set;



@NamedQueries({
        @NamedQuery(name="Album.findByOwner",
        query = "SELECT a FROM Album a WHERE a.owner=:owner"),

        @NamedQuery(name="Album.findAll",
        query = "SELECT a FROM Album a LEFT JOIN FETCH a.photos"),

        @NamedQuery(name="Album.findAllShared",
        query = "SELECT a FROM Album a JOIN a.shared u WHERE u.id=:id"),

        @NamedQuery(name="Album.findAlbumByGroups",
        query = "SELECT a FROM Groups g INNER JOIN g.albums a WHERE g.id=:id")
})

@Entity
public class Album {

    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message="Un nom d'album doit être renseigné")
    private String name;
    
    @NotBlank(message="Une description doit être renseignée")
    private String description;

    @ManyToOne
    @NotNull
    private User owner;

    @ManyToMany
    private Set<User> shared;

    @ManyToMany
    private Set<Groups> groups;
    
    @OneToMany(mappedBy = "album")
    private Set<Photo> photos;

    public Album(){}

    public Album(String name,  String description, User owner, Set<Photo> photos) {
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.photos = photos;
    }

    //GETTER AND SETTER --------------------------------------
	public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Set<Groups> getGroups() {
        return groups;
    }

    public void setGroups(Set<Groups> groups) {
        this.groups = groups;
    }

    public Set<User> getShared() {
		return shared;
	}

	public void setSharedWith(Set<User> shared) {
		this.shared = shared;
	}

    public Set<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(Set<Photo> photos) {
		this.photos = photos;
    }
    // FIN GETTER AND SETTER --------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return Objects.equals(name, album.name) &&
                Objects.equals(owner, album.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, owner);
    }
    
    public int nbPhotos() {
    	return this.photos.size();
    }
    
    public void addGroup(Groups group) {
    	this.groups.add(group);
    }
}
