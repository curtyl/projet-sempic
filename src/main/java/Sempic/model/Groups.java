package Sempic.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.Objects;
import java.util.Set;

@NamedQueries({
        @NamedQuery(name="Groups.findByOwner",
        query = "SELECT g FROM Groups g WHERE g.owner.id=:id"),

        @NamedQuery(name="Groups.findAllEagerMembers",
                query = "SELECT g FROM Groups g LEFT JOIN FETCH g.members")
})

@NamedEntityGraphs({
        // Exemple d'entity graph qui permet de ne charger que l'id et le nom d'un groupe
        @NamedEntityGraph(name = "Groups.groupOnly",
                attributeNodes = {
                @NamedAttributeNode("id"),
                @NamedAttributeNode("name")
            })
})
@Entity
public class Groups{

    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message="Il faut un nom de groupe")
    private String name;

    @ManyToOne
    @NotNull
    private User owner;

    @ManyToMany
    private Set<User> members;

    @ManyToMany
    private Set<Album> albums;

    public Groups() {

    }

    public Groups(String name, User owner) {
        this.name = name;
        this.owner = owner;
    }

    // GETTER AND SETTER --------------------------------------
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
        members.add(owner);
    }

    public Set<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(Set<Album> albums) {
        this.albums = albums;
    }

    // FIN GETTER AND SETTER --------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Groups group = (Groups) o;
        return Objects.equals(name, group.name) &&
                Objects.equals(owner, group.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, owner);
    }
}
