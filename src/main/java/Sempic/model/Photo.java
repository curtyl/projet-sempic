package Sempic.model;

import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;



@NamedQueries({
	@NamedQuery(name="Photo.findPhotosByAlbum",
		query = "SELECT p FROM Photo p JOIN p.album a WHERE a.id=:id"),

	@NamedQuery(name="Photo.findPhotoByAlbum",
		query = "SELECT p FROM Photo p JOIN p.album a WHERE a.id=:id AND p.id=:id"),

	@NamedQuery(name="Photo.findAllShared",
		query = "SELECT p FROM Photo p JOIN p.album a JOIN a.shared s WHERE s.id=:id")

})

@Entity
public class Photo {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotBlank(message = "Un titre doit être renseigné")
	private String title;
	
	@NotBlank(message = "Veuillez sélectionner le chemin du fichier")
	private String path;
	
	@ManyToOne
	private Album album;

	public Photo() {
	}

	// GETTER AND SETTER 
	public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
	}
	// FIN GETTER AND SETTER
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return Objects.equals(id, photo.id) &&
                Objects.equals(path, photo.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, path);
    }
}
