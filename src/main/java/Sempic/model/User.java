package Sempic.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueEmail", columnNames = { "email" }) })

@NamedQueries({
        @NamedQuery(name = "User.findAll", query = "SELECT DISTINCT u FROM User u LEFT JOIN FETCH u.groups LEFT JOIN FETCH u.memberOf"),

        @NamedQuery(name = "User.login", query = "SELECT DISTINCT u FROM User u WHERE u.email=:email"),

        @NamedQuery(name = "User.memberOf", query = "SELECT u FROM Groups g INNER JOIN g.members u WHERE g.id=:id") })
@NamedEntityGraph(name = "User.groups-memberOf", attributeNodes = { @NamedAttributeNode("groups"),
        @NamedAttributeNode("memberOf"), })
@Entity
public class User {
    // public final String PREFIX="/users/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "Un prénom doit être renseigné")
    private String firstname;

    @NotBlank(message = "Un nom doit être renseigné")
    private String lastname;

    @Email
    @NotBlank(message = "Une adresse mail doit être renseignée")
    private String email;

    @NotBlank(message = "Un mot de passe doit être renseigné")
    private String passwordHash;

    @Transient
    private String password;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE)
    private Set<Groups> groups;

    @ManyToMany(mappedBy = "members") // ,cascade = CascadeType.REMOVE)//, fetch=FetchType.EAGER
    private Set<Groups> memberOf;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "VARCHAR(5)")
    private UserType userType;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE)
    private Set<Album> albums;

    public User() {
        userType = UserType.USER;
    }

    public User(long id, UserType type) {
        this.id = id;
        this.userType = type;
    }

    // GETTER AND SETTER --------------------------------------
    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void setPassword(String p) {
        password = p;
    }

    public String getPassword() {
        return password;
    }

    public Set<Groups> getGroups() {
        if (groups==null) return Collections.emptySet();
        return Collections.unmodifiableSet(groups);
    }

    public Set<Groups> getMemberOf() {
        if (memberOf==null) return Collections.emptySet();
        return Collections.unmodifiableSet(memberOf);
    }

    public Set<Album> getAlbums() {
		return albums;
	}
    
    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
    // FIN GETTER AND SETTER --------------------------------------

	public void shareAlbum(Album album, Set<Groups> groups) {
		groups.forEach((Groups group) -> {
			album.addGroup(group);
		});
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
