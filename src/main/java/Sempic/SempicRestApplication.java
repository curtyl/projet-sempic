package Sempic;

import org.eclipse.microprofile.auth.LoginConfig;

import javax.annotation.security.DeclareRoles;
import javax.annotation.sql.DataSourceDefinition;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;



/**
 *
 */
@DataSourceDefinition(
        name = SempicRestApplication.DATA_SOURCE,
        // HSQL
        className = "org.hsqldb.jdbcDriver",
        //url="jdbc:hsqldb:mem:hsqldb", // memory database that is deleted when the server is stopped
        url = "jdbc:hsqldb:file:../app_database",
        databaseName = "monApplication",
        user = "MyAdm",
        password = "MyPass"
)

@LoginConfig(authMethod = "MP-JWT", realmName = "jwt-jaspi")
@DeclareRoles({"user","admin"})

@ApplicationPath("/data")
public class SempicRestApplication extends Application {

    public final static String DATA_SOURCE = "java:app/my_db";
   
}