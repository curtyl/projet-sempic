package Sempic.dao;

import java.io.StringWriter;
import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import Sempic.model.Album;
import Sempic.model.Photo;
import fr.uga.miashs.sempic.rdf.BasicSempicRDFStore;
import fr.uga.miashs.sempic.rdf.Namespaces;
import fr.uga.miashs.sempic.model.rdf.SempicOnto;


public class PhotosDao extends GenericJpaRestDao<Photo>{
    
    private BasicSempicRDFStore rdfStore = new BasicSempicRDFStore();

    public PhotosDao() {
		super(Photo.class);
    }
  
    public Response createPhoto(long albumId, Photo p, UriInfo uriInfo) {
        Album album = getEm().find(Album.class,albumId);
        if (album==null) throw new NotFoundException();
        p.setAlbum(album);
        Response r = create(p,uriInfo);
        long albumID = p.getAlbum().getId();
        long userID =  p.getAlbum().getOwner().getId();

        rdfStore.createPhoto(p.getId(), albumID, userID);
        return r;
    }

    public Response getPhotoShared(long albumId, long photoId, UriInfo uriInfo, int limit, int offset) {
        TypedQuery<Photo> q = getEm().createQuery("Photo.findAllShared", Photo.class);
        q.setParameter("id",albumId);
        q.setParameter("photoId",photoId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public Response getPhotos(long albumId, UriInfo uriInfo, int limit, int offset) {
        TypedQuery<Photo> q = getEm().createNamedQuery("Photo.findPhotosByAlbum", Photo.class);
        q.setParameter("id",albumId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public Response getPhoto(long albumId, long photoId, UriInfo uriInfo, int limit, int offset) {
        TypedQuery<Photo> q = getEm().createQuery("Photo.findPhotosByAlbum", Photo.class);
        q.setParameter("id",albumId);
        q.setParameter("photoId",photoId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public String addOwnerId(long photoId, List<String> obj) {
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        obj.forEach( o -> photoR.addProperty(SempicOnto.ownerId, ResourceFactory.createResource(o)));
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public String addTakenBy(long photoId, List<String> obj) {
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        obj.forEach( o -> photoR.addProperty(SempicOnto.takenBy, ResourceFactory.createResource(o)));
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public String addDepicts(long photoId, List<String> obj) {
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        //obj.forEach( o -> photoR.addProperty(SempicOnto.depicts, ResourceFactory.createResource(o)));
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }
}
