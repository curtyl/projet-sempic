package Sempic.dao;

import Sempic.model.Groups;
import Sempic.model.User;
import fr.uga.miashs.sempic.rdf.BasicSempicRDFStore;

import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;


public class GroupsDao extends GenericJpaRestDao<Groups> {

	private BasicSempicRDFStore RDFStore = new BasicSempicRDFStore();
	
    public GroupsDao() {
        super(Groups.class);
    }

    public Response addMembers(List<Long> ids, long groupId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(User.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
    	for(int i = 0; i < ids.size(); i++){
        	RDFStore.addUserGroup(ids.get(i), groupId);
        }
        Query q = getEm().createNativeQuery("INSERT INTO Groups_Users(Groups_id,Members_id) VALUES (?1,?2)");
        q.setParameter(1,groupId);
        ids.forEach( userId -> {
            q.setParameter(2,userId);
            q.executeUpdate();
            // La aussi meme avec em.getReference, il y a chargement des membres
            //em.find(Groups.class,g.id).getMembres().add(membre);
        });
        return Response.status(Response.Status.OK).build();
    }

    public Response deleteMembers(List<Long> ids, long groupId, UriInfo uriInfo) {
        Query q = getEm().createNativeQuery("DELETE FROM Groups_Users WHERE Groups_id=?1 AND Members_id=?2");
        q.setParameter(1,groupId);
        ids.forEach( userId -> {
            q.setParameter(2,userId);
            q.executeUpdate();
        });
        return Response.status(Response.Status.OK).build();
    }

    public Response createGroup(long userId, Groups g, UriInfo uriInfo) {
        User owner = getEm().find(User.class,userId);
        if (owner==null) throw new NotFoundException();
        g.setOwner(owner);
        //return create(g,uriInfo);
        
        
        
        Response r = super.create(g, uriInfo);
		Groups groupCreated = r.readEntity(Groups.class);
		RDFStore.createGroup(groupCreated.getName(), groupCreated.getId(), groupCreated.getOwner().getId());
		return r;
    }

    public Response getGroups(long userId, UriInfo uriInfo, int limit, int offset) {
        EntityGraph graph = getEm().getEntityGraph("Groups.groupOnly");
        TypedQuery<Groups> q = getEm().createNamedQuery("Groups.findByOwner", Groups.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",userId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    @Override
    public Response listAll(UriInfo uriInfo, int limit, int offset) {
        return super.executeQuery(getEm().createNamedQuery("Groups.findAllEagerMembers", Groups.class),
                uriInfo,limit,offset);
    }

    public Response addAlbums(List<Long> ids, long groupId, UriInfo uriInfo) {
    	for(int i = 0; i < ids.size(); i++){
        	RDFStore.addAlbumGroup(ids.get(i), groupId);
        }
        Query q = getEm().createNativeQuery("INSERT INTO Groups_Album(Groups_id,Albums_id) VALUES(?1,?2)");
        q.setParameter(1,groupId);
        ids.forEach( albumId -> {
            q.setParameter(2,albumId);
            q.executeUpdate();
        });
        return Response.status(Response.Status.OK).build();
    }

    public Response deleteAlbums(List<Long> ids, long groupId, UriInfo uriInfo) {
        Query q = getEm().createNativeQuery("DELETE FROM Groups_Album WHERE Groups_id=?1 AND Albums_id=?2");
        q.setParameter(1,groupId);
        ids.forEach( albumId -> {
            q.setParameter(2,albumId);
            q.executeUpdate();
        });
        return Response.status(Response.Status.OK).build();
    }


}
