package Sempic.dao;

import Sempic.JWT.JWTUtils;
import Sempic.model.User;
import Sempic.model.UserModelException;
import Sempic.model.UserType;
import fr.uga.miashs.sempic.rdf.BasicSempicRDFStore;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;



public class UsersDao extends GenericJpaRestDao<User> {
	private BasicSempicRDFStore RDFStore = new BasicSempicRDFStore();	
	@Inject
	private Pbkdf2PasswordHash hashAlgo;

	@Inject
	private JWTUtils jwtUtils;

	public UsersDao() {
		super(User.class);
	}

	public Response getGroupMembers(long groupId, UriInfo uriInfo, int limit, int offset) {
		TypedQuery<User> q = getEm().createNamedQuery("User.memberOf", User.class);
		q.setParameter("id",groupId);
		return executeQuery(q,uriInfo,limit,offset);
	}

	public Response login(String email, String password) {
		TypedQuery<User> q = getEm().createNamedQuery("User.login", User.class);
		q.setParameter("email", email);
		User u = q.getSingleResult();

		if(!hashAlgo.verify(password.toCharArray(),u.getPasswordHash())){
			throw new NotAuthorizedException("Mauvais login ou mot de passe");
		}

		List<String> roles = new ArrayList<>();
		roles.add("user");
		
		if (u.getUserType() == UserType.ADMIN) {
			roles.add("admin");
		} 

		String jwt = jwtUtils.generateToken(u.getEmail(), roles, 7200);
		return Response.ok().entity(jwt).build();
	}

	public Response listUsers(UriInfo uriInfo, int limit, int offset) {
		TypedQuery<User> q = getEm().createNamedQuery("User.findAll", User.class);
		return executeQuery(q,uriInfo,limit,offset);
	}

	@Override
	public Response create(User u, UriInfo uriInfo) {
		if (u.getPassword() != null) {
			u.setPasswordHash(hashAlgo.generate(u.getPassword().toCharArray()));
			u.setPassword(null);
		}else  {
			u.setPasswordHash(null); 
		}
		Response r = super.create(u, uriInfo);
		User userCreated = r.readEntity(User.class);
		RDFStore.createUser(userCreated.getId(), userCreated.getFirstname(), userCreated.getLastname(), userCreated.getEmail());
		//RDFStore.createTe();
		return r;
	
	}
	
	
	public Response deleteUser(long userId) {
		Query q = getEm().createNativeQuery("DELETE FROM USER WHERE id=?1");
        q.setParameter(1, userId);
        q.executeUpdate();
        return Response.status(Response.Status.OK).build();
	}

}
