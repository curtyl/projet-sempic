package Sempic.dao;


import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import fr.uga.miashs.sempic.rdf.BasicSempicRDFStore;

import Sempic.model.Album;
import Sempic.model.User;

public class AlbumsDao extends GenericJpaRestDao<Album> {

	private BasicSempicRDFStore RDFStore = new BasicSempicRDFStore();

    public AlbumsDao() {
		super(Album.class);
	}

	public Response getGroupAlbums(long groupId, UriInfo uriInfo, int limit, int offset) {
        TypedQuery<Album> q = getEm().createNamedQuery("Album.findAlbumByGroups", Album.class);
        q.setParameter("id",groupId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public Response createAlbum(long userId, Album a, UriInfo uriInfo) {
        User owner = getEm().find(User.class,userId);
        a.setOwner(owner);
        //return create(a,uriInfo);
        
        Response r = super.create(a, uriInfo);
		Album albumCreated = r.readEntity(Album.class);
		RDFStore.createAlbum(albumCreated.getName(), albumCreated.getId(), owner.getId());
		//RDFStore.createTe();
		return r;
    }

    public Response getAlbums(long userId, UriInfo uriInfo, int limit, int offset) {
        TypedQuery<Album> q = getEm().createNamedQuery("Album.findByOwner", Album.class);
        q.setParameter("id",userId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public Response getAlbum(long userId, long albumId, UriInfo uriInfo, int limit, int offset) {
        TypedQuery<Album> q = getEm().createQuery("SELECT a FROM Album a WHERE a.owner.id=:id AND a.id=:albumId", Album.class);
        q.setParameter("id",userId);
        q.setParameter("albumId",albumId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public Response deleteAlbum(long albumId) {
        Query q = getEm().createQuery("DELETE FROM Photo p WHERE p.album.id=?1");
        q.setParameter(1, albumId);
        q.executeUpdate();

        q = getEm().createQuery("DELETE FROM Album a WHERE a.id=?1");
        q.setParameter(1, albumId);
        q.executeUpdate();
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response listAll(UriInfo uriInfo, int limit, int offset) {
        return super.executeQuery(getEm().createNamedQuery("Album.findAll", Album.class),
                uriInfo,limit,offset);
    }

}
