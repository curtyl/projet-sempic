package Sempic.dao;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import javax.validation.constraints.PositiveOrZero;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/* Une seule instance est créée par l'environnement d'éxecution */
@Singleton
/* Chaque méthode = une transaction.
C'est obligé d'avoir des transactions pour JPA
*/
@Transactional
/**
 * Dao générique pour accéder à des entités JPA.
 * Les méthodes de ce DAO génèrent des réponses HTTP (javax.ws.rs.core.Response).
 * Les méthodes de base fournies sont les classiques CRUD (create, read, update, delete) ainsi qu'une méthode
 * listAll paginée, count, ainsi qu'une méthode permettant d'exécuter une requête retournant un sous ensemble
 * des entités (executeQuery).
 */
public abstract class GenericJpaRestDao<T> {

    @Inject
    private Validator validator;

    @PersistenceContext
    private EntityManager em;

    /**
     * Type de l'entité.
     * C'est utile de l'avoir pour JPA (find, TypedQueries, etc.)
     */
    private Class<T> clazz;

    /**
     * L'attribut de l'entité annoté avec @Id
     */
    private Field idField;

    // liste des attributs "non relation" (i.e. non annoté via ManyToOne, OneToMany, ManyToMany)
    private List<String> fieldNames;

    private Query deleteQuery;
    private TypedQuery<Long> countQuery;

    /**
     * Constucteur qui prend en paramètre la classe du type géré par le DAO.
     * Dans les sous classes, on définira un constructeur sans paramètre qui appellera
     * celui-ci avec la classe gérée en paramètre
     * @param clazz
     */
    public GenericJpaRestDao(Class<T> clazz) {
        this.clazz = clazz;

        // Recherche des attributs "relation", ie. ceux annotés avec
        // ManyToOne, OneToMany, ManyToMany, OneToOne
        fieldNames = new ArrayList<>();
        for (Field f : clazz.getDeclaredFields()) {
            if (f.isAnnotationPresent(Id.class)) {
                idField = f;
            }
            if (!(f.isAnnotationPresent(ManyToOne.class) ||
                    f.isAnnotationPresent(OneToMany.class) ||
                    f.isAnnotationPresent(ManyToMany.class) ||
                    f.isAnnotationPresent(OneToOne.class))) {
                fieldNames.add(f.getName());
            }

        }
    }

    @PostConstruct
    public void init() {
        // put here some initialisation that would be done after injection
    }

    private TypedQuery<Long> getCountQuery() {
        if (countQuery == null) {
            // query to count
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Long> cq = qb.createQuery(Long.class);
            cq.select(qb.count(cq.from(clazz)));
            countQuery = em.createQuery(cq);
        }
        return countQuery;
    }

    private Query getDeleteQuery() {
        if (deleteQuery == null) {
            StringBuilder sb = new StringBuilder("DELETE FROM ");
            sb.append(clazz.getSimpleName()).append(" WHERE ").append(idField.getName()).append(" = :id");
            deleteQuery = em.createQuery(sb.toString());
        }
        return deleteQuery;
    }

    public EntityManager getEm() {
        return em;
    }


    public long getId(T obj) {
        return (long) em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(obj);
    }

    /**
     * Comme l'annotation @Valid ne fonctionne pas avec des types génériques,
     * on peut utiliser ce palliatif si besoin
     */
    protected void validate(T obj) {
        Set<ConstraintViolation<T>> res = validator.validate(obj);
        if (res != null && !res.isEmpty()) {
            throw new ConstraintViolationException(res);
        }
    }

    
    public Response create(T obj, UriInfo uriInfo) {
        // inutile car persist check la validitÃ©
        //validate(obj);
        em.persist(obj);
        // le flush oblige a envoyer les modifs Ã  la BD
        // si la maj provoque un violation de contrainte d'intÃ©gritÃ©
        // alors une Exception du type PersitenceException est levÃ©e
        // si exception il y a celle ci est gÃ©rÃ©e via le ExceptionMapper "PersistenceExceptionMapper"
        em.flush();
        // on dÃ©tache l'entitÃ© - et on met null aux propriÃ©tÃ©s non chargÃ©es
        setNullForAllLazyLoadEntities(obj);
        getEm().detach(obj);

        URI u = uriInfo.getRequestUriBuilder().path(String.valueOf(getId(obj))).build();
        return Response.created(u).entity(obj).build();
    }

    /**
     * Méthode de lecture d'une entité.
     * Cette méthode prend en paramètre un "fetch graph"
     * afin de personnaliser le chargement de l'entité.
     * @param id
     * @param fetchGraph
     * @return
     */
    public T read(long id, EntityGraph<T> fetchGraph) {
        T res = em.find(clazz, id, Collections.singletonMap("javax.persistence.fetchgraph", fetchGraph));
        // on détache l'entité - et on met null aux propriétés non chargées
        setNullForAllLazyLoadEntities(res);
        getEm().detach(res);
        return res;
    }

    /**
     * Méthode de lecture d'une entité.
     * @param id
     * @return
     */
    public T read(long id) {
        T res = em.find(clazz, id);
        // on détache l'entité - et on met null aux propriétés non chargées
        setNullForAllLazyLoadEntities(res);
        return res;
    }

    /**
     * Méthode mise à jour (partielle) d'une entité.
     * Les information sont données dans un Map propriété -> valeur.
     * Le Map peut provenir d'un objet JSON.
     * @param id l'Id de l'entité à mettre à jour
     * @param obj Le map des propriété à mettre à jour
     * @param uriInfo
     * @return
     */
    public Response update(long id, Map<String, Object> obj, UriInfo uriInfo) {
        // Solution de update en une seule requête
        // mais pas de check sur les contraintes BeanValidation
        String idFieldName = idField.getName();
        obj.remove(idFieldName);
        // only retain information about our field names
        // remove the other for security reason (SQL injection)
        obj.keySet().retainAll(fieldNames);

        // construction de la requête update en SQL natif
        StringBuilder qs = new StringBuilder();
        qs.append("UPDATE ").append(clazz.getSimpleName()).append(" SET ");
        obj.keySet().forEach(k -> qs.append(k).append(" = :").append(k));
        qs.append(" WHERE ").append(idFieldName).append("=").append(id);

        Query q = em.createQuery(qs.toString());
        obj.forEach((k, v) -> q.setParameter(k, v));

        q.executeUpdate();

        return Response.ok().build();
        // solution qui produit 1 select + 1 full update
        // a l'avantage de vérifie rles contraintes BeanValidation
        /*T toUpdate = em.getReference(clazz,id);
        try {
            BeanUtils.copyProperties(toUpdate,obj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        // le flush oblige a envoyer les modifs à la BD
        // on peut donc récuperer l'exception via le ExceptionMapper
        em.flush();
        return Response.ok().entity(toUpdate).build();*/
    }

    /**
     * Méthode pour supprimer une entité.
     * @param id
     * @return
     */
    public Response delete(long id) {
        // Solution en SQL natif
        Query q = getDeleteQuery();
        q.setParameter("id", id);
        int nb = q.executeUpdate();
        if (nb == 0) return Response.status(Response.Status.NOT_FOUND).build();

        // Solution avec un select + delete
        // ca utilise JPA mais pas très intéressant
        /*em.remove(em.getReference(clazz,id));
        // le flush oblige a envoyer les modifs à la BD
        // on peut donc récuperer l'exception via le ExceptionMapper
        em.flush();*/
        return Response.noContent().build();
    }

    /**
     * Méthode retournant une liste (paginée) des entités.
     * @param uriInfo
     * @param limit le nombre max d'entités à retourner
     * @param offset la position de la prmière entité à retourner
     * @return
     */
    public Response listAll(UriInfo uriInfo, int limit, int offset) {
        CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(clazz);
        Root<T> c = cq.from(clazz);
        cq.select(c);

        TypedQuery<T> q = em.createQuery(cq);

        return executeQuery(q, uriInfo, limit, offset);
    }

    /**
     * Retourne le nombre d'entité de ce type dans la BD
     * @return
     */
    public long count() {
        return getCountQuery().getSingleResult();
    }

    /**
     * Méthode générique pour l'éxécution de requete retournant une liste d'entités.
     * La méthode supporte la pagination.
     * Cette méthode permet d'ajouter un lien vers
     * les pages suivante et précédente dans les entêtes de la réponse.
     * @param q
     * @param uriInfo
     * @param limit
     * @param offset
     * @return
     */
    public Response executeQuery(TypedQuery<T> q, UriInfo uriInfo, @PositiveOrZero int limit, @PositiveOrZero int offset) {
        if (limit != 0) {
            q.setMaxResults(limit);
        }
        if (offset >= 0) {
            q.setFirstResult(offset);
        }

        List<T> qRes = q.getResultList();
        // détachement "safe" des entités
        qRes.forEach(x -> setNullForAllLazyLoadEntities(x));
        qRes.forEach(x -> getEm().detach(x));

        Response.ResponseBuilder rb = Response
                .status(Response.Status.PARTIAL_CONTENT)
                .entity(qRes);

        // on ajoute des liens de navigation dans les entetes
        // ces liens permettent d'obtenir les resultats avant et après
        // il reste des elements ensuite
        //if (qRes.size()==limit) {
        long count = count();
        if (offset + qRes.size() < count) {
            rb.link(uriInfo.getRequestUriBuilder()
                    .replaceQueryParam("limit", Math.min(limit, count - offset))
                    .replaceQueryParam("offset", offset + qRes.size())
                    .build(), "next");
        }
        // il y a des elements avant
        if (offset > 0) {
            int computedMinOffset = Math.max(offset - qRes.size(), 0);
            rb.link(uriInfo.getRequestUriBuilder()
                    .replaceQueryParam("limit", offset - computedMinOffset)
                    .replaceQueryParam("offset", computedMinOffset)
                    .build(), "previous");
        }

        return rb.build();
    }

    /**
     * Méthode qui permet d'assigner "null" aux propriétés non chargées (lazy loading).
     * Cela évite d'avoir des LazyIntitializationException lorsque l'on accède
     * aux propriétés de l'entité en dehors de la transaction.
     * Cette méthode provoque le détachement de l'entité passée en paramètre.
     *
     * @param source l'entité à détacher
     */
    protected void setNullForAllLazyLoadEntities(Object source) {
        PersistenceUnitUtil unitUtil = getEm().getEntityManagerFactory().getPersistenceUnitUtil();
        //Logger.getAnonymousLogger().log(Level.WARNING,"setNullForAllLazyLoadEntities");
        for (Field field : source.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            getEm().detach(source);
            try {
                Object fieldValue = field.get(source);
                if (!unitUtil.isLoaded(fieldValue)) {
                    fieldValue = null;
                    field.set(source, fieldValue);
                }

                if (fieldValue != null) {
                    /*boolean isEntity = Arrays.asList(field.getType().getAnnotations())
                            .stream()
                            .filter(x -> x.annotationType().equals(Entity.class))
                            .count() > 0;*/
                    if (field.isAnnotationPresent(ManyToOne.class) || field.isAnnotationPresent(OneToOne.class)) {
                        setNullForAllLazyLoadEntities(fieldValue);
                    }
                    else if (field.isAnnotationPresent(ManyToMany.class) ||  field.isAnnotationPresent(OneToMany.class)) {
                        boolean isCollection = Arrays.stream(field.getType().getInterfaces())
                                .sequential()
                                .anyMatch( x-> x.equals(Collection.class));
                        if (isCollection) {
                            Collection<Object> c = (Collection<Object>) fieldValue;
                            c.forEach( x -> setNullForAllLazyLoadEntities(x));
                        }
                        else {

                            boolean isMap = Arrays.stream(field.getType().getInterfaces())
                                    .sequential()
                                    .anyMatch( x-> x.equals(Map.class));
                            if (isMap) {
                                Map<Object,Object> m = (Map<Object,Object>) fieldValue;
                                m.values().forEach( x -> setNullForAllLazyLoadEntities(x));
                            }
                            else {
                                Logger.getAnonymousLogger().log(Level.WARNING, field.getName() + " ignored");
                            }
                        }
                    }



                }
            } catch (IllegalAccessException e) {
                /*Errors errors = new Errors();
                errors.addMessage(e, "Failed to map field %s ", field.getType());
                throw new MappingException(errors.getMessages());*/
                throw new RuntimeException(e);
            }

        }
    }
}


