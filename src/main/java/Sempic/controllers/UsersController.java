package Sempic.controllers;

import Sempic.dao.AlbumsDao;
import Sempic.dao.GroupsDao;
import Sempic.dao.UsersDao;
import Sempic.model.Album;
import Sempic.model.Groups;
import Sempic.model.User;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.util.Map;

@Path("/users")
public class UsersController {

    @Inject
    private GroupsDao groupsDao;

    @Inject
    private UsersDao usersDao;

    @Inject
    private AlbumsDao albumsDao;

    @GET
    @Path("/login")
    public Response login(@QueryParam("email") String email, @QueryParam("password") String password) {
        return usersDao.login(email, password);
    }

    //1.1-	URL: /data/users/

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(User obj, @Context UriInfo uriInfo) {
        return usersDao.create(obj, uriInfo);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return usersDao.listAll(uriInfo, limit, offset);
    }

    //1.2-	URL: /data/users/{id: [0-9]+}

    @GET
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public User read(@PathParam("id") long id) {
        return usersDao.read(id);
    }

    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("id") long id) {
        return usersDao.deleteUser(id);
    }
    
    @PATCH
    @PUT
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Map<String, Object> obj, @Context UriInfo uriInfo) {
        return usersDao.update(id, obj, uriInfo);
    }

    //1.3-	URL: /data/users/{id: [0-9]+}/groups

    @POST
    @Path("/{id: [0-9]+}/groups")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createGroup(@PathParam("id") long userId, Groups g, @Context UriInfo uriInfo) {
        return groupsDao.createGroup(userId, g, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}/groups")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroups(@PathParam("id") long userId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return groupsDao.getGroups(userId, uriInfo, limit, offset);
    }

    //1.4-	URL: /data/users/{id: [0-9]+}/album

    @POST
    @Path("/{id: [0-9]+}/albums")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAlbum(@PathParam("id") long userId, Album a, @Context UriInfo uriInfo) {
        return albumsDao.createAlbum(userId, a, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}/albums")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAlbums(@PathParam("id") long userId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return albumsDao.getAlbums(userId, uriInfo, limit, offset);
    }

    //1.5-	URL : /data/users/{id: [0-9]+}/album/{albumId: [0-9]+}

    @GET
    @Path("/{id: [0-9]+}/albums/{albumId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAlbum(@PathParam("id") long userId, @PathParam("albumId") long albumId, @Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return albumsDao.getAlbum(userId, albumId, uriInfo, limit, offset);
    }
}
