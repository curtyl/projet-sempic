package Sempic.controllers;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import Sempic.dao.AlbumsDao;
import Sempic.dao.PhotosDao;
import Sempic.model.Photo;

@Path("/albums")
public class AlbumController {

    @Inject
    private AlbumsDao albumsDao;

    @Inject
    private PhotosDao photosDao;

    //2.1-	URL : /data/albums

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return albumsDao.listAll(uriInfo, limit, offset);
    }

    ///2.2-	URL : /data/albums/{id: [0-9]+}

    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAlbum(@PathParam("id") long id) {
        return albumsDao.deleteAlbum(id);
    }

    //2.3-	URL : /data/albums/{id: [0-9]+}/photos

    @POST
    @Path("/{id: [0-9]+}/photos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPhoto(@PathParam("id") long albumId, Photo p, @Context UriInfo uriInfo) {
        return photosDao.createPhoto(albumId, p, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}/photos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPhotos(@PathParam("id") long albumId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return photosDao.getPhotos(albumId, uriInfo, limit, offset);
    }

    //2.4-	URL : /data/albums/{id: [0-9]+}/photos/{photoId: [0-9]+}

    @GET
    @Path("/{id: [0-9]+}/photos/{photoId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPhoto(@PathParam("id") long albumId, @Context UriInfo uriInfo,
                             @PathParam("limit") int limit, @PathParam("offset") int offset, @PathParam("photoId") long photoId ) {
        return photosDao.getPhoto(albumId, photoId, uriInfo, limit, offset);
    }
}