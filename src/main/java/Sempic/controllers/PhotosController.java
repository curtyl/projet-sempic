package Sempic.controllers;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

import Sempic.dao.PhotosDao;
import fr.uga.miashs.sempic.rdf.BasicSempicRDFStore;

@Path("/photos")
public class PhotosController {

    @Inject
    private PhotosDao photosDao;

    private BasicSempicRDFStore rdfStore = new BasicSempicRDFStore();
    
    //4.1-	URL: /data/photos

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return photosDao.listAll(uriInfo, limit, offset);
    }

    //4.2-	URL: /data/photos/{id: [0-9]+}

    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletePhoto(@PathParam("id") long id) {
        rdfStore.deletePhoto(id);
        return photosDao.delete(id);
    }

    @POST
    @Path("/{photoId: [0-9]+}/depicts")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDepicts(@PathParam("photoId") long photoId, List<String> resources) {
        String res = photosDao.addDepicts(photoId, resources);
        return Response.ok(res).build();
    }

    @POST
    @Path("/{photoId: [0-9]+}/owner")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response hasAuthor(@PathParam("photoId") long photoId, List<String> resources) {
        String res = photosDao.addOwnerId(photoId, resources);
        return Response.ok(res).build();
    }

    @POST
    @Path("/{photoId: [0-9]+}/takenBy")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEvent(@PathParam("photoId") long photoId, List<String> resources) {
        String res = photosDao.addTakenBy(photoId, resources);
        return Response.ok(res).build();
    }
}
