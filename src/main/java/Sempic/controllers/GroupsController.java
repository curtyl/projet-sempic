package Sempic.controllers;

import Sempic.dao.AlbumsDao;
import Sempic.dao.GroupsDao;
import Sempic.dao.UsersDao;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/groups")
public class GroupsController {

    // On  peut injecter le JWT
    /*@Inject
    private JsonWebToken callerPrincipal;*/

    /*@Inject
    @Claim(standard = Claims.raw_token)
    private String rawToken;*/

    /*@Inject
    @Claim(standard = Claims.upn)
    private ClaimValue<String> emailUPN;*/

    @Inject
    private GroupsDao groupsDao;

    @Inject
    private UsersDao usersDao;

    @Inject
    private AlbumsDao albumsDao;

    //3.1-	URL: /data/groups

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return groupsDao.listAll(uriInfo, limit, offset);
    }

    //3.2-	URL: /data/groups/{id: [0-9]+}

    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteGroup(@PathParam("id") long id) {
        return groupsDao.delete(id);
    }

    //3.3-	URL: /data/groups/{id: [0-9]+}/members

    @GET
    @Path("/{groupId: [0-9]+}/members")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroupMembers(@PathParam("groupId") long groupId,@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return usersDao.getGroupMembers(groupId,uriInfo, limit, offset);
    }

    @POST
    @Path("/{groupId: [0-9]+}/members")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMembers(List<Long> ids, @PathParam("groupId") long groupId, @Context UriInfo uriInfo) {
        return groupsDao.addMembers(ids, groupId, uriInfo);
    }

    //3.4-	URL: /data/groups/{id: [0-9]+}/albums

    @POST
    @Path("/{groupId: [0-9]+}/albums")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addAlbums(List<Long> ids, @PathParam("groupId") long groupId, @Context UriInfo uriInfo) {
        return groupsDao.addAlbums(ids, groupId, uriInfo);
    }

    @GET
    @Path("/{groupId: [0-9]+}/albums")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroupAlbums(@PathParam("groupId") long groupId,@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return albumsDao.getGroupAlbums(groupId,uriInfo, limit, offset);
    }

    //3.5-	URL: /data/groups/{id: [0-9]+}/members/{userId: [0-9]+}

    @DELETE
    @Path("/{id: [0-9]+}/members/{userId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteMembers(@PathParam("userId") long userId, @PathParam("id") long groupId, @Context UriInfo uriInfo)
    {
        return groupsDao.deleteMembers(List.of(userId), groupId, uriInfo);
    }

    //3.5-	URL: /data/groups/{id: [0-9]+}/members/{albumId: [0-9]+}

    @DELETE
    @Path("/{id: [0-9]+}/albums/{albumId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAlbums(@PathParam("albumId") long albumId, @PathParam("id") long groupId, @Context UriInfo uriInfo)
    {
        return groupsDao.deleteAlbums(List.of(albumId), groupId, uriInfo);
    }
}

